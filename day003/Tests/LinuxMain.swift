import XCTest

import day003Tests

var tests = [XCTestCaseEntry]()
tests += day003Tests.allTests()
XCTMain(tests)
