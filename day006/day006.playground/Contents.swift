import UIKit

// Variables
var name = "Phil"
name = "Philip"

// Constants
let nameCons = "Phil"

// Type annotation
var nameEmtpy: String

var age: Int

// FLoat and Double
var latitude: Double
latitude = 36.1666667

var longitude: Float
longitude = -86.783333

// Boolean
var stayUpTooLate: Bool
stayUpTooLate = true

var nameSpecific: String = "Philip"

// Array
var songs = [String]()


// Store any type
var anyArray: [Any]

// Add data
songs.append("Shake it Off")

// Concat of array possible songs + songs2

// Dictionaries
var person = [
    "first": "Taylor",
    "middle": "Alison",
    "last": "Swift",
    "month": "December",
    "website": "taylorswift.com"
]
person["middle"]
person["month"]

var people = ["players", "haters", "heart-breakers", "fakers"]
var actions = ["play", "hate", "break", "fake"]
for i in 0 ..< people.count {
    print("\(people[i]) gonna \(actions[i])")
}

for action in actions {
    if action == "play" {
        continue
    }
    
    print(action)
}
