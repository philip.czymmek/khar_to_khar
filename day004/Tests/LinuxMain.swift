import XCTest

import day004Tests

var tests = [XCTestCaseEntry]()
tests += day004Tests.allTests()
XCTMain(tests)
