// MARK: - Structs Syntax

// Creating structs
struct Sport {

    var name: String
}

var tennis = Sport(name: "Tennis")
print(tennis.name)

tennis.name = "Lawn tennis"
print(tennis.name)

// Computed properties
struct SportComputed {

    var name: String
    var isOlympicSport: Bool

    var olympicStatus: String {
        if isOlympicSport {
            return "\(name) is an Olympic sport"
        } else {
            return "\(name) is not an Olympic sport"
        }
    }
}
let chessBoxing = SportComputed(name: "Chessboxing", isOlympicSport: false)
print(chessBoxing.olympicStatus)

// Property observers
struct Progress {

    var task: String
    var amount: Int {
        willSet {
            print("\(task) was at \(amount)% complete")
        }
        didSet {
            print("\(task) is now \(amount)% complete")
        }
    }
}
var progress = Progress(task: "Loading data", amount: 0)
progress.amount = 30
progress.amount = 80
progress.amount = 100

// Struct methods
struct City {

    var population: IntegerLiteralType

    func collectTaxes() -> Int {
        return population * 1000
    }
}
let london = City(population: 9_000_000)
print(london.collectTaxes())

// Mutating methods
struct Person {

    var name: StringLiteralType

    mutating func makeAnonymous() {
        name = "Anonymous"
    }
}
var person = Person(name: "Ed")
print(person.name)
person.makeAnonymous()
print(person.name)

// Properties and methods of strings
let string = "Do or do not, there is no try."
print(string.count)
print(string.hasPrefix("Do"))
print(string.uppercased())
print(string.sorted())

// Properties and methods of arrays
var toys = ["Woody"]
print(toys.count)
toys.append("Buzz")
toys.firstIndex(of: "Buzz")
print(toys.sorted())
toys.remove(at: 0)

// Initializers
struct Users {

    var username: String

    init() {
        username = "Anonymous"
        print("Creating a new user!")
    }
}
var users = Users()
print(users.username)
users.username = "Ed"
print(users.username)

// refering to an instance - self
struct Baby {

    var name: String

    init(name: String) {
        print("\(name) was born!")
        self.name = name
    }
}
var baby = Baby(name: "Jorn")

// Lazy properties
struct FamilyTree {

    init() {
        print("Creating family tree")
    }
}

struct LazyPerson {

    var name: String
    lazy var familyTree = FamilyTree()

    init(name: String) {
        self.name = name
    }
}
var edPerson = LazyPerson(name: "Ed")
// todo: XCode works, VS Code not
//edPerson.familyTree

// Static properties and methods
struct StudentStatic {

    static var classSize = 0
    var name: String

    init(name: String) {
        self.name = name
        StudentStatic.classSize += 1
    }
}
let staticEd = StudentStatic(name: "Ed")
let staticTaylor = StudentStatic(name: "Taylor")
print(StudentStatic.classSize)

// Access control
struct PersonWithAccess {

    private var id: String

    init(id: String) {
        self.id = id
    }

    func identifyPerson() -> String {
        return "Social secuity number is \(id)"
    }
}
let citizen = PersonWithAccess(id: "12344")
print(citizen.identifyPerson())

// MARK: - CLASSES

// Creating classes
class Dog {

    var name: String
    var breed: String

    init(name: String, breed: String) {
        self.name = name
        self.breed = breed
    }
}
let poppy = Dog(name: "Poppy", breed: "Poddle")

// Class inheritance
class Poddle: Dog {

    init(name: String) {
        super.init(name: name, breed: "Poddle")
    }
}

// Overriding methods
class Spaceship {

    func thrust() {
        print("Thrusting")
    }
}

class UFO: Spaceship {

    override func thrust() {
        print("Pew Pew Lightspeed")
    }
}

// Final class
// No more inheriting from this class
final class Rocket: Spaceship {

    var name: String
    var weight: Int

    init(name: StringLiteralType, weight: Int) {
        self.name = name
        self.weight = weight
    }
}
