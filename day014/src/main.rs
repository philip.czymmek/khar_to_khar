fn find_largest<T>(list: &[T]) -> &T {
    let mut largest = list[0];
    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 100, 65];
    let result = find_largest(&number_list);
    println!("The largest number is {}.", result);

    let number_list = vec![102, 34, 6000, 89, 54, 2, 43, 8];

    let result = find_largest(&number_list);
    println!("The largest number is {}", result);

    let tweet = Tweet {
        username: String::from("horse_ebook"),
        content: String::from("of course, as you probably know, people"),
        metadata: TweetType::TWEET,
    };

    println!("1 new tweet: {}", tweet.summarize());

    let article = NewsArticle {
        headline: String::from("Penguins win the Stanley Cup Championship!"),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from(
            "The Pittsburgh Penguins once again are the best \
             hockey team in the NHL.",
        ),
    };

    println!("New article available! {}", article.summarize());
}

pub trait Summary {
    fn summarize(&self) -> String;

    fn summarize_author(&self) -> String;

    fn read_more(&self) -> String {
        format!("(Read more from {}...)", self.summarize_author())
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {}, ({})", self.headline, self.author, self.location)
    }

    fn summarize_author(&self) -> String {
        format!("{}", self.author)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    metadata: TweetType,
}

enum TweetType {
    TWEET,
    RETWEET,
    REPLY,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }

    fn summarize_author(&self) -> String {
        format!("@{}", self.username)
    }
}
