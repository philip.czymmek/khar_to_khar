// MARK: - CLASSES

// Creating classes
class Dog {

    var name: String
    var breed: String

    init(name: String, breed: String) {
        self.name = name
        self.breed = breed
    }
}
let poppy = Dog(name: "Poppy", breed: "Poddle")

// Class inheritance
class Poddle: Dog {

    init(name: String) {
        super.init(name: name, breed: "Poddle")
    }
}

// Overriding methods
class Spaceship {

    func thrust() {
        print("Thrusting")
    }
}

class UFO: Spaceship {

    override func thrust() {
        print("Pew Pew Lightspeed")
    }
}

// Final class
// No more inheriting from this class
final class Rocket: Spaceship {

    var name: String
    var weight: Int

    init(name: StringLiteralType, weight: Int) {
        self.name = name
        self.weight = weight
    }
}

// Copying objects
class Singer {
    var name = "Hani"
}
var singer = Singer()
print(singer.name)

// Create a copy of the first object
var betterCopySinger = singer
betterCopySinger.name = "Phil rocks"
print(singer.name)

// Deinitializers
class DPerson {

    var name = "John Doe"

    init() {
        print("\(name) is alive!")
    }

    deinit {
        print("\(name) is no more!")
    }

    func printGreeting() {
        print("Hello, I am \(name)")
    }
}

for _ in 1...3 {
    let person = DPerson()
    person.printGreeting()
}

// Mutability
// Creating a constant class, properties are still changable
let person1 = DPerson()
person1.name = "Dieter"
print(person1.name)

// To stop that from happening
class Singer1 {

    let name = "Roger Bunny"
}

/*
    SUMMARY
    1. Classes and structs are similar -> own types with properties and methods
    2. One class can inherit from another, gains all properties and methods of parent class
    3. final keyword, stop other classes from inheriting
    4. override keyword, replace a method in its parent class with a new implementation
    5. when two variables point to the same class instance, they both point at the same piece of memory
    6. Classes have deinitializer, gets run when class is destroyed
    7. Classes don't enforce constants as strongly as structs
*/

// MARK: - PROTOCOLS

//Basics
protocol Identifiable {

    var id: String { get set }
}

struct User: Identifiable {

    var id: String
}

func diplayID(thing: Identifiable) {
    print("My ID is \(thing.id)")
}

// Protocol Inheritance
protocol Payable {

    func calculateWages() -> Int
}

protocol NeedsTraining {

    func study()
}

protocol HasVacation {

    func takeVacation(days: Int)
}

// Bring them together
protocol Employee: Payable, NeedsTraining, HasVacation { }

// Extensions
extension Int {

    func squared() -> Int {
        return self * self
    }
}

let number = 8
print(number.squared())

// Swift doesn't let you add stored properties in extensions, use computed properties instead
extension Int {

    var isEven: Bool {
        return self % 2 == 0
    }
}
print(number)
print(number.isEven)

// Protocol extensions
// Instead of affecting only one data type, we extend a protocol, all confiming types get the changes
let pythons = ["Eric", "Graham", "John", "Michael", "Terry", "Terry"]
let beatles = Set(["John", "Paul", "George", "Ringo"])

// Extend the Collection protocol
extension Collection {

    func summarize() {
        print("There are \(count) of us:")

        for name in self {
            print(name)
        }
    }
}

pythons.summarize()
beatles.summarize()

// Protocol-oriented programming
// Setup
struct User1: Identifiable_1 {

    var id: String
}

// Create protocol
protocol Identifiable_1 {

    var id: String { get set }

    func identify()
}

// Create confirming extension for type
extension Identifiable_1 {

    func identify() {
        print("My ID is \(id)")
    }
}
var twostraws = User1(id: "1234")
twostraws.identify()

/* 
    SUMMARY
    1. Protocols describe what methods and properties a conforming type must have, without providing implementations
    2. Protocols can be build on top of each other
    3. Extensions let you add methods and computed properties to specific types such as Int
    4. Protocol extensions let you add methods and computed properties to protocols
    5. Protocol-oriented programming is the practice of designing your app architecture as a series of protocols,
       then using protocol extensions to provide default method implementations
*/

// MARK: - Optionals

// Handling missing data
var age: Int? = nil
age = 38

// Unwrapping optionals - if let
var name: String?
if let unwrapped = name {
    print("\(unwrapped.count) letters")
} else {
    print("Missing name")
}

// Unwrapping with guard
func greet(_ name: String?) {
    guard let unwrapped = name else {
        print("You didn't provide a name!")
        return
    }

    print("Hello, \(unwrapped)")
}
greet(name)

// Force unwrapping - `!` convert from optional type to non-optional type
let str = "5"
let num = Int(str)!
print(type(of: num))

// Implicitly unwrapped optionals
// Usecase: varaible start life as `nil`, but will ALWAYS have a value before use
let ageAgain: Int! = nil

// Nil coalescing
func username(for id: Int) -> String? {
    if id == 1 {
        return "Big Boi Phil"
    } else {
        return nil
    }
}
let user = username(for: 15) ?? "Anonymous"

// Optional chaining
// a.b.c where b is optinal -> a.b?.c
let names = ["John", "Paul", "Ringo", "George"]
let beatle = names.first?.uppercased()
print(beatle!)

// Optional try
enum PasswordError: Error {

    case obvious
}

func checkPassword(_ password: String) throws -> Bool {
    if password == "password" {
        throw PasswordError.obvious
    }

    return true
}

// Option 1:
if let result = try? checkPassword("password") {
    print("Result was \(result)")
} else {
    print("D'oh.")
}

// Option 2:
if try! checkPassword("sekrit") {
    print("OK!")
}

// Failable initializers
struct PiPaPerson {
    var id: String

    init?(id: String) {
        if id.count == 9 {
            self.id = id
        } else {
            return nil
        }
    }
}

// Typecasting
class Animal { }
class Fish: Animal { }

class Doge: Animal {
    func makeNoise() {
        print("Woof")
    }
}
let pets = [Fish(), Doge(), Fish(), Doge()]
for pet in pets {
    if let dog = pet as? Doge {
        dog.makeNoise()
    }
}

/*
    SUMMARY
    1. Optionals represent the absence of a value in a clear and unambigious way
    2. Optionals have to be unwrapped, either `if let` or `guard let`
    3. Force unwrap with ! - only if safe
    4. Implicity unwrapped optionals don't have the safety checks of the regular optionals
    5. use nil coalescing to unwrap an optional and provide a default value if there was nothing inside
    6. Optional chaining let's us write code to manipulate an optional, but if optional == nil, ignore code
    7. try? to convert throwing function into an optional return value, or try! to crash if error is thrown
    8. If initializer needs to fail, use `init?()` to make failable initializer
    9. Typecasting to convert one type of object into another
*/
