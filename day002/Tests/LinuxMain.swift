import XCTest

import day002Tests

var tests = [XCTestCaseEntry]()
tests += day002Tests.allTests()
XCTMain(tests)
